;;;; Copyright (c) Eric Diethelm 2018 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license.

(in-package :trivial-continuation)

(defclass continuation-result ()
  ((result :documentation "The *RESULT* slot carries the return value from calling a continuation."
	   :reader result
	   :initarg :result
	   :initform nil)
   (continuation :documentation "The *CONTINUATION* slot holds the continuation function to be called next." 
		 :reader continuation
		 :initarg :continuation
		 :initform nil
		 :type (or null function))
   (operation :documentation "The *OPERATION* slot controls how two continuations are to be combined.  
Valid values are:  
  :combine - the current continuation carries a valid result, and still other continuations should be called  
  :delegate - the current continuation does not have any result, but still another continuation should be called  
  :terminate - no further continuation shall be called"
	      :reader operation
	      :initarg :operation
	      :initform :terminate
	      :type (member :terminate   ;; Stop looking for another result
			    :combine     ;; Take my result and combine with other potential continuations
			    :delegate    ;; Ignore my result and jump directly to the next continuation
			    )))
  (:documentation "The *CONTINUATION-RESULT* class represents the result of a method, the continuation from it and the combination operation."))

(defvar +continue-to-next+ (make-instance 'continuation-result :operation :delegate))
(defvar +continuation-failed+ (make-instance 'continuation-result :operation :terminate :result nil :continuation #'(lambda () +continuation-failed+)))

(defmethod print-object ((var continuation-result) out)
  (print-unreadable-object (var out :type nil)
    (format out "Continuation-Result (")
    (print-object (result var) out)
    (format out " | ")
    (print-object (continuation var) out)
    (format out " | ")
    (print-object (operation var) out)
    (format out ")")))

(defmethod trivial-utilities:clone ((obj continuation-result) &key &allow-other-keys)
  "Make a deep copy of the given *continuation-result."
  (make-instance 'continuation-result
		 :result (result obj)
		 :continuation (continuation obj)
		 :operation (operation obj)))

(defun follow-chain (result)
  "Return whether *RESULT* has a valid continuation."
  (and result (continuation result) (not (eq (operation result) :terminate))))

(defun rewrite-terminators (name body)
  "Run over *BODY* adding (:named-define name) to every cc/{delegate|terminate|create-return|return}."
  (iterate:iterate
     (iterate:for part in body)
     (iterate:collecting (cond ((atom part)
				part)
			       ((consp part)
				(if (member (car part) '(cc/delegate cc/terminate cc/create-return cc/return))
				    (append part (list :named-define name))
				    (rewrite-terminators name part)))))))

(defmacro cc/define (name (&rest args) &body body)
  "Define a function to serve as a continuation."
  `(defun ,name (,@args) ,@(rewrite-terminators name body)))

(defmacro cc/define-named-lambda (name (&rest args) &body body)
  "Define a named lambda to serve as a continuation."
  `(labels ((,name (,@args) ,@(rewrite-terminators name body)))
     #',name))

(defmacro cc/define-lambda ((&rest args) &body body)
  "Define a lambda to serve as a continuation."
  `(trivial-utilities:alambda (,@args)
     ,@body))

(defmacro cc/delegate (&key (named-define nil))
  "Indicate that this does not carry any result, but instead a continuation should be called. Used inside of *CC/DEFINE*."
  `(return-from ,(if named-define named-define (intern (string 'self)))
     (the (values trivial-continuation:continuation-result &optional) +continue-to-next+)))

(defmacro cc/terminate (&key (named-define nil))
  "Indicate that this result should be the last in a chain. Used inside of *CC/DEFINE*."
  `(return-from ,(if named-define named-define (intern (string 'self)))
     (the (values trivial-continuation:continuation-result &optional) +continuation-failed+)))

(defmacro cc/create-return (value continuation &key (operation :combine) (named-define nil))
  "Create a new result to be returned from inside *CC/DEFINE*."
  `(return-from ,(if named-define named-define (intern (string 'self)))
     (the (values trivial-continuation:continuation-result &optional)
	  (make-instance 'continuation-result
			 :result ,value
			 :continuation ,continuation
			 :operation ,operation))))

(defmacro cc/return (result &key (named-define nil))
  "Return the *RESULT* from inside of *CC/DEFINE*."
  `(return-from ,(if named-define named-define (intern (string 'self)))
     (the (values trivial-continuation:continuation-result &optional) ,result)))

(defun combine (first-cont next-cont)
  (declare (type (or null function) first-cont)
	   (type (or null function continuation-result) next-cont))

  (cc/define-lambda ()
      (trivial-utilities:aif (and first-cont (funcall first-cont))
			     (ecase (operation it)
			       (:terminate                                       ;; Stop looking for another result
				(cc/return it))
			       (:combine                                         ;; Take my result and combine with other potential continuations
				(setf (slot-value it 'continuation)
				      (combine (continuation it) next-cont))
				(cc/return it))
			       (:delegate                                        ;; Ignore my result and jump directly to the next continuation
				(if (continuation it)
				    (cc/return (call-continuation it))
				    (if next-cont
					(if (typep next-cont 'function)
					    (cc/return (funcall next-cont))
					    (cc/return next-cont))
					(cc/delegate)))))

			     (if next-cont
				 (if (typep next-cont 'function)
				     (cc/return (funcall next-cont))
				     (cc/return next-cont))
				 (cc/terminate)))))

(defgeneric combine-continuations (head tail)
  (:documentation "Combine two continuations to form a new one. *HEAD* and *TAIL* can (independently) be *CONTINUATION-RESULT* or a function returning a *CONTINUATION-RESULT*"))

(defmethod combine-continuations ((head continuation-result) (tail continuation-result))
  (if (eq (operation head) :delegate)
      (error "Combining a Result that is set to delegate is not possible"))

  (make-instance 'continuation-result
		 :result (result head)
		 :continuation (combine (continuation head) tail)
		 :operation :combine))

(defmethod combine-continuations ((head continuation-result) (tail function))
  (if (eq (operation head) :delegate)
      (error "Combining a Result that is set to delegate is not possible"))
  
  (make-instance 'continuation-result
		 :result (result head)
		 :continuation (combine (continuation head) tail)
		 :operation :combine))

(defmethod combine-continuations ((head function) (tail continuation-result))
  (combine-continuations (funcall head) tail))

(defmethod combine-continuations ((head function) (tail function))
  (combine-continuations (funcall head) tail))

(defun call-continuation (cont)
  "Call the continuation of a *CONTINUATION-RESULT*, checking if one is defined. Emits a warning when calling on an object with *OPERATION* :terminate."
  (declare (type continuation-result cont))

  (trivial-utilities:aif (continuation cont)
			 (funcall it)
			 +continuation-failed+))

(defun cc/continue (cont)
  "Call the continuation of a *CONTINUATION-RESULT*, checking if one is defined and modifying *CONT*. Emits a warning when calling on an object with *OPERATION* :terminate."
  (declare (type continuation-result cont))

  (when (eq (operation cont) :terminate)
    (log:warn "Applying 'call-continuation' on a terminated continuation!"))

  (trivial-utilities:aprog1
      (trivial-utilities:aif (continuation cont)
			     (funcall it)
			     +continuation-failed+)
    (setf (slot-value cont 'result) (result it))
    (setf (slot-value cont 'continuation) (continuation it))
    (setf (slot-value cont 'operation) (operation it)))
  (return-from cc/continue cont))

