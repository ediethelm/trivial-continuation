;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-


(defsystem :trivial-continuation
  :name "trivial-continuation"
  :description "Provides an implementation of function call continuation and combination."
  :version "0.1.5"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :in-order-to ((test-op (test-op :trivial-continuation/test)))
  :depends-on (:trivial-utilities
	       :log4cl)
  :components ((:file "package")
	       (:file "trivial-continuation")))

(defsystem :trivial-continuation/test
  :name "trivial-continuation/test"
  :description "Unit Tests for the trivial-continuation project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-continuation
	       :fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :trivial-continuation-tests))
  :components ((:file "test-trivial-continuation")))

