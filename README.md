# Trivial Continuation Manual

###### \[in package TRIVIAL-CONTINUATION\]
[![pipeline status](https://gitlab.com/ediethelm/trivial-continuation/badges/master/pipeline.svg)](https://gitlab.com/ediethelm/trivial-continuation/commits/master)
[![Quicklisp](http://quickdocs.org/badge/trivial-continuation.svg)](http://quickdocs.org/trivial-continuation/)
[![coverage report](https://gitlab.com/ediethelm/trivial-continuation/badges/master/coverage.svg?job=test-coverage)](https://gitlab.com/ediethelm/trivial-continuation/-/jobs/artifacts/master/browse?job=test-coverage)

## Description

This library provides an implementation of function call continuation and combination.

## Installing trivial-continuation

This project is available in the latest [QuickLisp](https://www.quicklisp.org/beta/ "QuickLisp") distribution, so installing it is reduced to calling:

```lisp
(ql:quickload :trivial-continuation)
```


## Working Example

```lisp
(cc/define simple-continuation (seq)
  (if seq
      cc/create-return (car seq)
			#'(lambda () (simple-continuation (cdr seq)))
			:operation :combine)
      (cc/delegate)))

(let ((cont (combine-continuations (simple-continuation '(1 2 3 4))  
                                   (simple-continuation '(5 6 7 8)))))
  (format t \"~a\" (result cont))

  (setf cont (call-continuation cont))
  (format t \"~a\" (result cont)))

```


## Exported Symbols

- [class] CONTINUATION-RESULT

    The *CONTINUATION-RESULT* class represents the result of a method, the continuation from it and the combination operation.

- [reader] RESULT CONTINUATION-RESULT (:RESULT = NIL)

    The *RESULT* slot carries the return value from calling a continuation.

- [reader] CONTINUATION CONTINUATION-RESULT (:CONTINUATION = NIL)

    The *CONTINUATION* slot holds the continuation function to be called next.

- [reader] OPERATION CONTINUATION-RESULT (:OPERATION = :TERMINATE)

    The *OPERATION* slot controls how two continuations are to be combined.  
    Valid values are:  
      :combine - the current continuation carries a valid result, and still other continuations should be called  
      :delegate - the current continuation does not have any result, but still another continuation should be called  
      :terminate - no further continuation shall be called

- [macro] CC/DEFINE (&REST ARGS) &BODY BODY

    Define a function to serve as a continuation.

- [macro] CC/RETURN RESULT

    Return the *RESULT* from inside of *CC/DEFINE*.

- [macro] CC/CREATE-RETURN VALUE CONTINUATION &KEY (OPERATION :COMBINE)

    Create a new result to be returned from inside *CC/DEFINE*.

- [macro] CC/DELEGATE 

    Indicate that this does not carry any result, but instead a continuation should be called. Used inside of *CC/DEFINE*.

- [macro] CC/TERMINATE 

    Indicate that this result should be the last in a chain. Used inside of *CC/DEFINE*.

- [function] CC/CONTINUE CONT

    Call the continuation of a *CONTINUATION-RESULT*, checking if one is defined and modifying *CONT*. Emits a warning when calling on an object with *OPERATION* :terminate.

- [generic-function] COMBINE-CONTINUATIONS HEAD TAIL

    Combine two continuations to form a new one. *HEAD* and *TAIL* can (independently) be *CONTINUATION-RESULT* or a function returning a *CONTINUATION-RESULT*

- [function] CALL-CONTINUATION CONT

    Call the continuation of a *CONTINUATION-RESULT*, checking if one is defined. Emits a warning when calling on an object with *OPERATION* :terminate.

- [function] FOLLOW-CHAIN RESULT

    Return whether *RESULT* has a valid continuation.

## License Information

This library is released under the MIT License. Please refer to the [LICENSE](https://gitlab.com/ediethelm/trivial-continuation/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/ediethelm/trivial-continuation/blob/master/CONTRIBUTING.md "Contributing") document for more information.

