

(in-package :trivial-continuation)

(5am:def-suite :trivial-continuation-tests :description "Trivial Continuation tests")
(5am:in-suite :trivial-continuation-tests)

(defmacro create-walk (seq abort)
  `(cc/define-lambda (&key (current-count 0))
     (if (>= current-count (length ,seq))
	 ,abort
	 (cc/create-return (nth current-count ,seq)
			   #'(lambda () (self :current-count (1+ current-count)))
			   :operation :combine))))

(5am:test simple-chain
  (let* ((continuation nil)
	 (rand '(6 4 7 9 2 5)))
    (setf continuation (funcall (create-walk rand (cc/terminate))))
    (5am:is (eq (result continuation) (nth 0 rand)))

    (iterate:iterate
      (iterate:for i from 1 below (length rand))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i rand))))

    (setf continuation (call-continuation continuation))
    (5am:is (eq (result continuation) nil))
    (5am:is (eq (operation continuation) :terminate))))

(5am:test delegate-chain
  (let ((continuation nil)
	(seq-a '(5 9 2 4 7 3 4))
	(seq-b '(7 6 1 6 4 8 5 3 5)))
    (labels ((walk (seq)
	       (cc/define-lambda (&key (current-count 0))
		(if (>= current-count (/ (length seq) 2))
		    (cc/delegate)
		    (cc/create-return (nth current-count seq)
				      #'(lambda () (self :current-count (1+ current-count)))
				      :operation :combine)))))

    (setf continuation (combine-continuations (funcall (walk seq-a))
					      (funcall (walk seq-b))))
    (5am:is (eq (result continuation) (nth 0 seq-a)))

    (iterate:iterate
      (iterate:for i from 1 below (/ (length seq-a) 2))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-a))))

    (iterate:iterate
      (iterate:for i from 0 below (/ (length seq-b) 2))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-b))))

    (setf continuation (call-continuation continuation))
    (5am:is (eq (operation continuation) :delegate)))))

(5am:test terminate-chain
  (let ((continuation nil)
	(seq-a '(5 9 2 4 7 3 4))
	(seq-b '(7 6 1 6 4 8 5 3 5)))

    (setf continuation (combine-continuations (funcall (create-walk seq-a (cc/terminate)))
					      (funcall (create-walk seq-b (cc/terminate)))))
    (5am:is (eq (result continuation) (nth 0 seq-a)))

    (iterate:iterate
      (iterate:for i from 1 below (length seq-a))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-a))))

    (setf continuation (call-continuation continuation))
    (5am:is (eq (operation continuation) :terminate))))

(5am:test combine/result-result
  (let ((continuation nil)
	(seq-a '(5 9 2 4 7 3 4))
	(seq-b '(7 6 1 6 4 8 5 3 5)))

    (setf continuation (combine-continuations  (funcall (create-walk seq-a (cc/delegate)))
					       (funcall (create-walk seq-b (cc/delegate)))))
    (5am:is (eq (result continuation) (nth 0 seq-a)))

    (iterate:iterate
      (iterate:for i from 1 below (length seq-a))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-a))))

    (iterate:iterate
      (iterate:for i from 0 below (length seq-b))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-b))))

    (setf continuation (call-continuation continuation))
    (5am:is (eq (operation continuation) :delegate))))

(5am:test combine/result-function
  (let ((continuation nil)
	(seq-a '(7 5 6 3 4 6 5 2))
	(seq-b '(8 6 3 7 4 8 6 7 8)))

    (setf continuation (combine-continuations  (funcall (create-walk seq-a (cc/delegate)))
					       (create-walk seq-b (cc/terminate))))
    (5am:is (eq (result continuation) (nth 0 seq-a)))

    (iterate:iterate
      (iterate:for i from 1 below (length seq-a))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-a))))

    (iterate:iterate
      (iterate:for i from 0 below (length seq-b))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-b))))

    (setf continuation (call-continuation continuation))
    (5am:is (eq (operation continuation) :terminate))))

(5am:test combine/function-function
  (let ((continuation nil)
	(seq-a '(6 4 7 1 5))
	(seq-b '(9 4 6 8)))
    (setf continuation (combine-continuations  (create-walk seq-a (cc/delegate))
					       (create-walk seq-b (cc/delegate))))
    (5am:is (eq (result continuation) (nth 0 seq-a)))

    (iterate:iterate
      (iterate:for i from 1 below (length seq-a))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-a))))

    (iterate:iterate
      (iterate:for i from 0 below (length seq-b))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-b))))

    (setf continuation (call-continuation continuation))
    (5am:is (eq (operation continuation) :delegate))))

(5am:test combine/function-result
  (let ((continuation nil)
	(seq-a '(7 3 2 4 4 7))
	(seq-b '(5 1 7 5 6)))
    (setf continuation (combine-continuations  (create-walk seq-a (cc/delegate))
					       (funcall (create-walk seq-b (cc/terminate)))))
    (5am:is (eq (result continuation) (nth 0 seq-a)))

    (iterate:iterate
      (iterate:for i from 1 below (length seq-a))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-a))))

    (iterate:iterate
      (iterate:for i from 0 below (length seq-b))
      (setf continuation (call-continuation continuation))
      (5am:is (eq (result continuation) (nth i seq-b))))

    (setf continuation (call-continuation continuation))
    (5am:is (eq (operation continuation) :terminate))))

