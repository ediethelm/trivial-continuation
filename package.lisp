(uiop:define-package #:trivial-continuation
  (:documentation "trivial-continuation provides an implementation of function call continuation and combination.")
  (:use #:common-lisp)
  (:export #:continuation-result
	   #:result
	   #:continuation
	   #:operation
	   #:follow-chain
	   #:cc/define
	   #:cc/define-named-lambda
	   #:cc/define-lambda
	   #:cc/return
	   #:cc/create-return
	   #:cc/delegate
	   #:cc/fail
	   #:cc/continue
	   #:cc/terminate
	   #:combine-continuations
	   #:call-continuation))

